terraform {
  backend "s3" {
    region  = "eu-west-3"
    //Creer le bucket avant de lancer le script et changer le nom ici (bucket a nom unique)
    bucket  = "biologist-mood-wp"
    key     = "wordpress.tfstate"
    encrypt = true
    //profile = "terraform"
  }
}
