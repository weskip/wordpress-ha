resource "aws_db_instance" "Biologist-mood-RDS" {
  allocated_storage    = var.db_storage
  engine               = var.db_engine
  engine_version       = "10.5.12"
  instance_class       = "db.t3.micro"
  db_subnet_group_name = aws_db_subnet_group.db_private_subnet.name
  name                 = "${var.project}-RDS"
  username             = var.db_username
  password             = var.db_password
  max_allocated_storage = 1000 
  multi_az              = false

  tags = {
      Name = "${var.project}-RDS"
      Contact = var.Contact
  }
}
////////////////////////////////////////////////////////////////////////////////////

resource "aws_db_subnet_group" "db_private_subnet" {
  name       = "db-subnet-group"
  subnet_ids = [aws_subnet.prv1.id, aws_subnet.prv2.id]

  tags = {
    Name = "${var.project}-DB-subnet-group"
    Contact = var.Contact
  }
}
/////////////////////////////////////////////////////////////////////////////////

resource "aws_security_group" "DB_security_group" {
  name        = "DB_security_group"
  description = "Allow EC2 to connect on port 3306"
  vpc_id      = aws_vpc.biologist_mood_vpc.id

  ingress {
    description      = "3306 from EC2"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    security_groups = [aws_security_group.ec2-sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project}-DB_security_group"
    Contact = var.Contact
  }
}