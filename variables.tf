variable "aws_region" {
  default = "eu-west-3"
}

variable "cidr_vpc" {
  default = "10.0.0.0/16"
}

variable "project" {}
variable "Contact" {}


variable "db_username"{
    type = string
    default     = ""
    description = "Username to connect to RDS"
}
variable "db_password" {
  type        = string
  default     = ""
  description = "Password to connect to RDS"
}


variable "db_storage"{
    type = number
}
variable "db_engine" {
    default = "mariadb"
    type = string
}