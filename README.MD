Terraform Project to host a HA wordpress website based on nginx webserver

################ Prerequisites ################

Install AWS CLI V2
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

Install Terraform 
1st/ Install Chocolatey
https://chocolatey.org/

2nd/ Install Terraform
choco install terraform

################ Execution ################

Creer un nouveau bucket et changer le nom (biologist-mood-wp) dans le fichier avant de lancer l'Execution
Dans le repo du Projet 
Terraform init
Terraform apply --auto-approve

################ Infrastructure ################

Compute 
EC2 instances 
Autoscaling group
ALB

DB
RDS : Mariadb

Network 
VPC
2 Public Subnet on 2 AZ
2 Private Subnet on 2 AZ
