############# VPC ############

resource "aws_vpc" "biologist_mood_vpc" {
  cidr_block           = var.cidr_vpc
  enable_dns_hostnames = true
  tags = {
    Name    = "${var.project}-vpc"
    Project = var.project
  }
}

############# Subnet Public ############


resource "aws_subnet" "pub1" {
  vpc_id     = aws_vpc.biologist_mood_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name    = "${var.project}-public1"
    Project = var.project
  }
}
resource "aws_subnet" "pub2" {
  vpc_id     = aws_vpc.biologist_mood_vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name    = "${var.project}-public2"
    Project = var.project
  }
}


############# Subnet Privé ############

resource "aws_subnet" "prv1" {
  vpc_id     = aws_vpc.biologist_mood_vpc.id
  cidr_block = "10.0.10.0/24"

  tags = {
    Name    = "${var.project}-prive1"
    Project = var.project
  }
}

resource "aws_subnet" "prv2" {
  vpc_id     = aws_vpc.biologist_mood_vpc.id
  cidr_block = "10.0.20.0/24"

  tags = {
    Name    = "${var.project}-prive2"
    Project = var.project
  }
}

####################### IGW #################################

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.biologist_mood_vpc.id

  tags = {
    Name    = "${var.project}-igw"
    Project = var.project
  }
}

####################### NAT--GW + EIP #################################

resource "aws_eip" "eip" {
  vpc = true
  tags = {
    Name    = "${var.project}-EIP"
    Project = var.project
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.pub1.id

  tags = {
    Name    = "${var.project}-nat-gateway"
    Project = var.project
  }

  depends_on = [aws_internet_gateway.gw]
}

################### Route Table Public + Associate + routes #####################

resource "aws_route_table" "Routing-Table-Public" {
  vpc_id = aws_vpc.biologist_mood_vpc.id

  tags = {
    Name    = "${var.project}-public-RT"
    Project = var.project
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.pub1.id
  route_table_id = aws_route_table.Routing-Table-Public.id
  }

  resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.pub2.id
  route_table_id = aws_route_table.Routing-Table-Public.id
  }
resource "aws_route" "public_route" {
  route_table_id            = aws_route_table.Routing-Table-Public.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
}

################### Route Table Private + Associate + routes #####################


resource "aws_route_table" "Routing-Table-Private" {
  vpc_id = aws_vpc.biologist_mood_vpc.id

  tags = {
    Name    = "${var.project}-private-RT"
    Project = var.project
  }
}

resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.prv1.id
  route_table_id = aws_route_table.Routing-Table-Private.id
  }

  resource "aws_route_table_association" "d" {
  subnet_id      = aws_subnet.prv2.id
  route_table_id = aws_route_table.Routing-Table-Private.id
  }

  resource "aws_route" "private_route" {
  route_table_id            = aws_route_table.Routing-Table-Private.id
  destination_cidr_block    = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.nat.id
}

